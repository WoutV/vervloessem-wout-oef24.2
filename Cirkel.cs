﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Vervloessem_Wout_oef24._2
{
    class Cirkel
    {
        private double _straal;
        public double Straal
        {
            get { return _straal; }

            set { if(value<0){
                    MessageBox.Show("Gelieve een positief getal in te geven");


                }
                else
                {
                    Straal = value;
                }
            }
            
        }
        public double BerekenOmtrek()
        {
          
            return Straal * Math.PI * 2; ;
        }
        public double BerekenOppervlakte()
        {
            return Math.Pow(Straal,2) * Math.PI ;

        }
        public Cirkel(double straal)
        {
            Straal = straal;

        }
        public string FormattedOpperVlakte()
        {           
            return String.Format("{0:0.00}", BerekenOppervlakte()); 
        }
        public string FormattedOmtrek()
        {
            return String.Format("{0:0.00}", BerekenOmtrek()); 
        }
        public override string ToString()
        {
          return  "Omtrek:                " + FormattedOmtrek() + Environment.NewLine +
            "Oppervlakte:           " + FormattedOpperVlakte();
          
        }
    }
}
